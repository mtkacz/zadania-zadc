# -*- coding: utf-8 -*-

import json
import string
from MapReduce import MapReduce


def map(item):
    val=[]
    val.append(item[0])
    for it in item[2:]:
        val.append(it)
    yield item[1], val

def reduce(key, values):
    ore = ()
    lin = ()
    vale = []
    for val in values:
        if val[0] == "order":
            ore = json.dumps(val)
        else:
            lin = json.dumps(val)
            vale.append(ore + " " + lin)
    return (key, vale)

if __name__ == '__main__':
    input = []
    with open('records.json', 'rt') as f:
        for li in f:
            input.append(json.loads(li))
    #input_data = json.loads(input)
    mapper = MapReduce(map, reduce)
    results = mapper(input)
    ite = 1
    for row in results:
        lin = "[\""+row[0]+"\", "
        for ro in row[1]:
            te = ""
            for r in ro:
                if r == ']':
                    if ite == 2:
                        print lin+te+']'
                        ite = 1
                    else:
                        ite = 2
                elif r != '[':
                    te += r