import sys
import couchdb
import tailer
from p6 import apachelog

couch = couchdb.Server('http://194.29.175.241:5984')
db = couch["p6"]

def parse(filename):
    format = r'%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"'
    parser = apachelog.parser(format)

    with open(filename, 'r') as file:
        for line in file:
            try:
                yield parser.parse(line)
            except:
                sys.stderr.write("Problem z parsowaniem %s" % line)

entries = parse("/var/log/apache2/access.log")
print len(list(db))
