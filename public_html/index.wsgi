# -*- coding: utf-8 -*-
import os
import requests
# This is our application object. It could have any name,
# except when using mod_wsgi where it must be "application"
import json
from multiprocessing import Pool
from collections import defaultdict
from itertools import chain, repeat
from operator import itemgetter
import sys

def sort(list):
    result = []
    #counter = 0
    for item in sorted(list.items(), key=lambda x: x[1], reverse=True):
       # if counter < n:
       result.append(item)
        #    counter+=1
    return result

def map(item):
    yield (item['ip'],item['bytes'])

def reduce(key, values):
    result = dict()
    for item in values:
        result[key]=result.get(key, 0)+int(item)
    return result

def reduce_connections(key, values):
    result = dict()
    for item in values:
        result[key]=result.get(key,0)+1
    return result

def map_wrapper(args):
    data = args[0]
    map = args[1]
    return [x for x in map(data)]


def reduce_wrapper(args):
    data = args[0]
    reduce = args[1]
    return reduce(data[0], data[1])


def reduce_identity(key, values):
    return (key, values)


class MapReduce(object):

    def __init__(self, map_func, reduce_func=reduce_identity, num_workers=None):
        """Konstruktor MapReduce

        map_func
            Funkcja mapujÄ.ca (item) -> None, wykorzystujÄ.ca konstrukcjÄ.
            yield key, value
            do wygenerowania dowolnej liczby mapowa..

        reduce_func=reduce_identity
            Funkcja redukujÄ.ca (key, values) -> object, zwracajÄ.ca
            zagregowane dane dla danego klucza. Domy..lnie ustawiona jest funkcja,
            kt..ra zwraca otrzymane argumenty.

        num_workers=None
            Liczba proces..w wykonujÄ.cych zadanie.
            W przypadku braku parametru liczba ta jest automatycznie ustawiana
            na sumarycznÄ. liczbÄ. rdzeni wszystkich procesor..w.
        """
        self.map_func = map_func
        self.reduce_func = reduce_func
        self.pool = Pool(num_workers)

    def __partition(self, mapped_values):
        """Organizuje zmapowane warto..ci po kluczu.
        Zwraca nieposortowanÄ. sekwencjÄ. krotek z kluczem i sekwencjÄ. warto..ci.
        """
        partitioned_data = defaultdict(list)
        for key, value in mapped_values:
            partitioned_data[key].append(value)
        #sorted(partitioned_data.items(), key=labda x:x[0], reverse=True)
        return partitioned_data.items()

    def __call__(self, inputs, chunksize=1):
        """Przetwarza dane wej..ciowe z u..yciem funkcji map i reduce.

        inputs
            Struktura iterowalna zawierajÄ.ca dane wej..ciowe do przetworzenia.

        chunksize=1
            Rozmiar danych wej..ciowych, kt..re majÄ. byÄ. na raz przekazane do jednego procesu.
        """
        map_responses = self.pool.map(map_wrapper, zip(inputs, repeat(self.map_func)), chunksize=chunksize)
        partitioned_data = self.__partition(chain(*map_responses))
        reduced_values = self.pool.map(reduce_wrapper, zip(partitioned_data, repeat(self.reduce_func)))
        return reduced_values

def application( # It accepts two arguments:
      # environ points to a dictionary containing CGI like environment variables
      # which is filled by the server for each received request from the client
      environ,
      # start_response is a callback function supplied by the server
      # which will be used to send the HTTP status and headers to the server
      start_response):
   r = requests.get("http://194.29.175.241:5984/p1/_all_docs")
   # build the response body possibly using the environ dictionary
   response_body = '<table border=1><tr><td>IP</td><td>BYTES</td></tr>'
   resp = r.json['rows']
   ids = list()
   data = list()
   for item in resp:
        ids.append(str(item['id']))
   for id in ids:
        request = requests.get("http://194.29.175.241:5984/p1/"+id)
        data.append(request.json)

   mapper = MapReduce(map,reduce)
   res = mapper(data)

   for item in res:
        for key in item:
            response_body +="<tr><td>%s</td><td>%d</td></tr>"%(str(key), item.get(key))
   response_body += "</table>"
   response_body += "<table border=1><tr><td>IP</td><td>CONNECTIONS</td></tr>"
   mapper2 = MapReduce(map,reduce_connections)
   res2 = mapper2(data)
   #re = sort(res2)
   for item in res2:
        for key in item:
            response_body +="<tr><td>%s</td><td>%d</td></tr>"%(str(key), item.get(key))
   response_body += "</table>"

   # HTTP response code and message
   status = '200 OK'

   # These are HTTP headers expected by the client.
   # They must be wrapped as a list of tupled pairs:
   # [(Header name, Header value)].
   response_headers = [('Content-Type', 'text/html'),
                       ('Content-Length', str(len(response_body)))]

   # Send them to the server using the supplied function
   start_response(status, response_headers)

   # Return the response body.
   # Notice it is wrapped in a list although it could be any iterable.
   return [response_body]


